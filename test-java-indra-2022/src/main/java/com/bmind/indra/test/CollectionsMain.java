package com.bmind.indra.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Predicate;

import org.apache.commons.collections4.ListUtils;
import org.apache.commons.collections4.MapUtils;
import org.apache.commons.collections4.SetUtils;

public class CollectionsMain {

	public static void main(String[] args) {
		CollectionsMain main = new CollectionsMain();
		//main.listas();
		//main.set();
		//main.mapa();
		main.collections();
	}
	
	public void listas() {
		Random random = new Random(System.currentTimeMillis());
		
		List<Integer> myList = new ArrayList<>();
		
		myList.add(random.nextInt(100));
		myList.add(random.nextInt(100));
		myList.add(random.nextInt(100));
		myList.add(random.nextInt(100));
		
		/*for(Integer num : myList) {
			System.out.println("Número: " + num);
		}*/
		
		myList.forEach((num) -> {
			System.out.println("Número: " + num);
		});
		
		myList.set(1, 5);
		Integer num2 = myList.get(1);
		
		Predicate<Integer> myPredicate = num -> num > 50;
		
		myList.removeIf(num -> num > 50);
		myList.removeIf(myPredicate);
		
		myList.forEach(num -> System.out.println("Número: " + num));
		
		LinkedList<Integer> linked = new LinkedList<>();
	}
	
	public void set() {
		Random random = new Random(System.currentTimeMillis());
		
		Set<Integer> numeros = new HashSet<>();
		
		numeros.add(200);
		numeros.add(random.nextInt(100));
		numeros.add(random.nextInt(100));
		numeros.add(random.nextInt(100));
		numeros.add(random.nextInt(100));
		numeros.add(200);
		
		numeros.forEach((num) -> {
			System.out.println("Número: " + num);
		});
		
		LinkedHashSet<Integer> linked = new LinkedHashSet<>();
		
		TreeSet<Integer> tree = new TreeSet();
		//tree.
	}
	
	public void mapa() {
		Random random = new Random(System.currentTimeMillis());
		
		Map<Integer, String> myMap = new HashMap<>();
		
		myMap.put(random.nextInt(100), "A");
		myMap.put(random.nextInt(100), "B");
		myMap.put(random.nextInt(100), "C");
		myMap.put(random.nextInt(100), "D");
		
		myMap.keySet().forEach(key -> 
			System.out.println("Key: " + key + " Value: " + myMap.get(key)));
		
		myMap.values().forEach(value -> System.out.println("Value: " + value));
		
		myMap.entrySet().forEach(entry -> {
			System.out.println("Key: " + entry.getKey() + 
					" Value: " + entry.getValue());
		});
		
		myMap.getOrDefault(100, "Cadena por defecto");
		
		//LinkedHashMap<K, V>;
		//TreeMap<K, V>;
	}
	
	public void collections() {
		//ListUtils
		//MapUtils.
		//SetUtils.
	}
	
}
