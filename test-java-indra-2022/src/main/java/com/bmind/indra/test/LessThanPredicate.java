package com.bmind.indra.test;

import java.util.function.Predicate;

public class LessThanPredicate implements Predicate<String> {

	@Override
	public boolean test(String t) {
		return t.length() < 5;
	}

}
