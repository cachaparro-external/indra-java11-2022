package com.bmind.indra.test.clone;

public class Precio implements Cloneable{

	private Double valor;
	private String moneda;
	public Double getValor() {
		return valor;
	}
	public void setValor(Double valor) {
		this.valor = valor;
	}
	public String getMoneda() {
		return moneda;
	}
	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		Precio otherPrecio = new Precio();
		otherPrecio.setMoneda(this.moneda);
		otherPrecio.setValor(this.valor);
		return otherPrecio;
	}
}
