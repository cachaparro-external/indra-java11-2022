package com.bmind.indra.test.clone;

public class Product implements Cloneable {

	private String nombre;
	private String marca;
	private Precio precio;
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public Precio getPrecio() {
		return precio;
	}
	public void setPrecio(Precio precio) {
		this.precio = precio;
	}
	@Override
	protected Object clone() throws CloneNotSupportedException {
		//Copia superficial
		/*Product other = new Product();
		other.setMarca(this.marca);
		other.setNombre(this.nombre);
		other.setPrecio(this.precio);*/
		
		//Copia profunda
		Product other = new Product();
		other.setMarca(this.marca);
		other.setNombre(this.nombre);
		other.setPrecio((Precio)this.precio.clone());
		
		return other;
	}
}
