package com.bmind.indra.test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.bmind.indra.test.products.Drink;
import com.bmind.indra.test.products.DrinkComparator;
import com.bmind.indra.test.products.Food;

public class TestMain {

	public static void main(String[] args) {
		/*int i1 = 25;
		long l1 = 30L;
		
		float f1 = 3.5F;
		double d1 = 5.8;
		
		int ib1 = 0b101;
		int io1 = 052;
		int ih1 = 0x2A;
		
		char c1 = 'A';
		char c2 = 80;
		
		System.out.println(c1);
		System.out.println(c2);
		System.out.println((int)c1);*/
		
		/*int a1 = 5;
		int a2 = ~a1; // - (5 + 1)
		
		System.out.println(a2);*/
		
		/*Product product = ProductFactoryMethod.getProduct(1);
		product.printAccion();
		product.printMarca();
		
		if(product instanceof Drink) {
			((Drink)product).printTipoBebida();
		}*/
		
		/*IMascota mascota = new Perro();
		mascota.printTipoMascota();
		
		if(mascota instanceof Perro) {
			((Perro)mascota).ladrar();
		}*/
		
		/*com.bmind.indra.test.clone.Product product = new com.bmind.indra.test.clone.Product();
		
		Precio precio = new Precio();
		precio.setMoneda("COP");
		precio.setValor(100.0);
		
		product.setMarca("Coca Cola");
		product.setNombre("Gaseosa");
		product.setPrecio(precio);*/
		
		//product.
		
		
		Food food = new Food();
		food.setMarca("Coca cola");
		food.setTamano(1L);
		
		Food otherFood = new Food();
		food.setMarca("Pepsi");
		food.setTamano(2L);
		
		int resultado = food.compareTo(otherFood);
		//1 Objeto food es mayor que otherFood
		//-1 Objeto food es menor que otherFood
		//0 Objeto food es igual que otherFood
		
		
		Drink drink = new Drink();
		food.setMarca("Coca cola");
		food.setTamano(1L);
		
		Drink otherDrink = new Drink();
		food.setMarca("Pepsi");
		food.setTamano(2L);
		
		
		DrinkComparator drinkComparator = new DrinkComparator();
		int resultado1 = drinkComparator.compare(drink, otherDrink);
		
		List<Drink> lista = new ArrayList<>();
		lista.add(otherDrink);
		lista.add(drink);
		
		Collections.sort(lista, drinkComparator);
		
		Collections.sort(lista, new Comparator<Drink>() {

			@Override
			public int compare(Drink arg0, Drink arg1) {
				if(arg0.getMarca().equals(arg0.getMarca())) {
					return 0;
				}else {
					return 1;
				} 	
			} 
		});
		
		Collections.sort(lista, (Drink d1, Drink d2) -> {
			return d1.getMarca().equals(d2.getMarca()) ? 0 : -1;
		});
		
		Collections.sort(lista, (final Drink d1, final Drink d2) -> {
			return d1.getMarca().equals(d2.getMarca()) ? 0 : -1;
		});
		
		Collections.sort(lista, (var d1, var d2) -> {
			return d1.getMarca().equals(d2.getMarca()) ? 0 : -1;
		});
		
		Collections.sort(lista, (d1, d2) -> {
			return d1.getMarca().equals(d2.getMarca()) ? 0 : -1;
		});
		
		Collections.sort(lista, (d1, d2) -> d1.getMarca().equals(d2.getMarca()) ? 0 : -1);
		
		
		Comparator<Integer> higherThan = (num1, num2) -> num1.compareTo(num2);
		Comparator<Integer> lessThan = (num1, num2) -> num2.compareTo(num1);
		
		Collections.sort(new ArrayList<Integer>(), higherThan.thenComparing(higherThan).reversed());
		Collections.sort(new ArrayList<Integer>(), Comparator.naturalOrder());
		Collections.sort(new ArrayList<Integer>(), Comparator.reverseOrder());
		Collections.sort(new ArrayList<Integer>(), Comparator.nullsFirst(higherThan));
		Collections.sort(new ArrayList<Integer>(), Comparator.nullsLast(lessThan));
		
		
	}

}
