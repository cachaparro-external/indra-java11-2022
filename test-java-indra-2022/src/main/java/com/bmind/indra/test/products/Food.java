package com.bmind.indra.test.products;

public class Food /*extends Product*/ implements Comparable<Food>{

	private String marca;
	private Long tamano;
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public Long getTamano() {
		return tamano;
	}
	public void setTamano(Long tamano) {
		this.tamano = tamano;
	}
	@Override
	public int compareTo(Food arg0) {
		if(arg0 == null) {
			return 1; 
		}
		
		if(this.marca.equals(arg0.getMarca())) {
			return 0;
		}else {
			return 1;
		}
	}
	
	/*@Override
	public void printAccion() {
		System.out.println("Comiendo");
	}*/
}
