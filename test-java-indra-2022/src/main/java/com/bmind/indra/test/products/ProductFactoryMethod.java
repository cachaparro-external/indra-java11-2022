package com.bmind.indra.test.products;

public class ProductFactoryMethod {
	
	private ProductFactoryMethod() {
		
	}
	
	public static Product getProduct(int option) {
		if(option == 1) {
			//return new Drink();
			return null;
		}else if(option == 2) {
			//return new Food();
			//return new Drink();
			return null;
		}else {
			throw new IllegalArgumentException(String.format("Opción %d es invalida", option));
		}
	}

}
