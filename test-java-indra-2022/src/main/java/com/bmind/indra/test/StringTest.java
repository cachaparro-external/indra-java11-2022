package com.bmind.indra.test;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;
import java.util.concurrent.TimeUnit;

public class StringTest {

	public static void main(String[] args) {
		/*String cadena1 = "Cadena de prueba";
		String cadena2 = "Cadena de prueba";
		String cadena3 = cadena2;
		String cadena4 = new String("Cadena de prueba");
		String cadena5 = new String("Cadena de prueba");
		
		System.out.println(cadena4 == cadena5);
		System.out.println(cadena4.equals(cadena5));*/
		
		//Crear fecha
		LocalDate ld1 = LocalDate.MIN;
		LocalDate ld2 = LocalDate.MAX;
		LocalDate ld3 = LocalDate.EPOCH;
		LocalDate ld4 = LocalDate.now();
		
		LocalDateTime ldt1 = LocalDateTime.now(ZoneId.of("America/Los_Angeles"));
		LocalDateTime ldt2 = LocalDateTime.now();
		
		System.out.println(ZoneOffset.of("-05:00"));
		System.out.println(Duration.between(ldt1, ldt2).get(ChronoUnit.DAYS));
		
		
		
		
	}
	
}
