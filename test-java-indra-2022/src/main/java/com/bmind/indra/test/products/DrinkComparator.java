package com.bmind.indra.test.products;

import java.util.Comparator;

public class DrinkComparator implements Comparator<Drink> {

	@Override
	public int compare(Drink arg0, Drink arg1) {
		if(arg0 == null && arg1 == null) {
			return 0;
		}
		
		if(arg0.getMarca().equals(arg0.getMarca())) {
			return 0;
		}else {
			return 1;
		}
	}

}
