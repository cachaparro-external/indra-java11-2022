package com.bmind.indra.test;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntConsumer;
import java.util.function.IntSupplier;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.function.UnaryOperator;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamTest {

	public static void main(String[] args) {
		StreamTest main = new StreamTest();
		//main.stream1();
		//main.stream2();
		//main.stream3();
		//main.straem4();
		main.stream5();
	}
	
	public void stream1() {
		List<String> cadenas = Arrays.asList("Pera", "Manzana", "Papaya", "Higo", "Melon");
		
		//Forma 1
		/*LessThanPredicate predicate = new LessThanPredicate();
		
		cadenas.stream()
			.filter(predicate)
			.forEach((fruta) -> {
				System.out.println("Fruta: " + fruta);
			});*/
		
		//Forma 2
		/*Predicate<String> predicate2 = (cadena) -> {
			return cadena.length() >= 5; 
		};
		
		Predicate<String> predicate3 = cadena -> cadena.length() >= 5;
		
		cadenas.stream()
			.filter(predicate3)
			.forEach(fruta -> System.out.println("Fruta: " + fruta));*/
		
		//Forma 3
		/*cadenas.stream()
			.filter(cadena -> cadena.length() == 5)
			.forEach(System.out::println);*/
		
		Predicate<String> lessThan = fruta -> fruta.length() <= 6;
		Predicate<String> greatherThan = fruta -> fruta.length() > 4;
		
		/*cadenas.stream()
			.filter(lessThan)
			//.peek(System.out::println)
			.filter(greatherThan)
			.forEach(System.out::println);*/
		
		cadenas.stream()
			.filter(lessThan.and(greatherThan))
			.forEach(System.out::println);
		
		System.out.println("----------------");
		
		cadenas.stream()
			.filter(lessThan.or(greatherThan))
			.forEach(System.out::println);
		
		System.out.println("----------------");
		
		cadenas.stream()
			.filter(lessThan.negate())
			.forEach(System.out::println);
		
		System.out.println("----------------");
		cadenas.stream()
			.filter(Predicate.isEqual("Pera"))
			.forEach(System.out::println);
		
		
		//cadenas.removeIf(greatherThan)
	}
	
	public void stream2() {
		List<String> cadenas = Arrays.asList("Pera", "Manzana", "Papaya", "Higo", "Melon");
		
		/*Function<String, Integer> getCantidadCaracteres = (cadena) -> {
			return cadena.length();
		};
		
		Function<String, Integer> getCantidadCaracteres1 = cadena -> cadena.length();
		
		cadenas.stream()
			.map(getCantidadCaracteres1)
			.forEach(System.out::println);	
		
		cadenas.forEach(cadena -> System.out.println(cadena));*/
		
		cadenas.stream()
			.mapToInt(cadena -> {
				Integer cantidad = 0;
				
				for(char caracter : cadena.toUpperCase().toCharArray()) {
					switch (caracter) {
					case 'A':
					case 'E':
					case 'I':
					case 'O':
					case 'U':
						cantidad++;
						break;
					}
				}
				
				return cantidad;
			})
			.forEach(System.out::println);
	}
	
	public void stream3() {
		List<String> cadenas = Arrays.asList("Pera", "Manzana", "Papaya", "Higo", "Melon");
		
		/*UnaryOperator<String> toUpper = cadena -> cadena.toUpperCase();
		
		cadenas.stream()
			.map(toUpper)
			.forEach(System.out::println);
		
		System.out.println("-----------------------");
		
		cadenas.forEach(cadena -> System.out.println(cadena));*/
		
		/*cadenas.stream()
			.map(cadena -> cadena.toLowerCase())
			.forEach(System.out::println);*/
		
		/*cadenas.stream()
			.map(String::toLowerCase)
			.forEach(System.out::println);*/
		
		List<Integer> numeros = Arrays.asList(1, 3, 5, 7);
		
		numeros.stream()
			.map(num -> num + 5)
			.forEach(System.out::println);
		
		System.out.println("----------------");
		
		numeros.forEach(num -> System.out.println(num));

	}
	
	public void straem4() {
		Integer[][] numeros = {
				{1, 9}, 
				{2, 8}, 
				{3, 7}};
		
		/*Arrays.stream(numeros) // [{1, 9}, {2, 8}, {3, 7}]
			.flatMap(numero -> Arrays.stream(numero)) //[1, 9, 2, 8, 3, 7]
			.forEach(System.out::println);*/
		
		Integer[] arregloDeUnaDimension = Stream.of(numeros)
			.flatMap(Stream::of)
			.toArray(Integer[]::new);
		
		for(Integer num : arregloDeUnaDimension) {
			System.out.println(num);
		}
	}
	
	public void stream5() {
		IntSupplier generarAleatorio = () -> {
			Random random = new Random(System.currentTimeMillis());
			
			return random.nextInt(100);
		};
		
		IntConsumer impresion = numero -> System.out.println("Número: " + numero);
		
		IntStream.generate(generarAleatorio)
			.forEach(impresion);
	}

}
